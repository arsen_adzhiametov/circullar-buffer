import java.util.Arrays;

public class Buffer {

    private volatile Integer[] array = new Integer[5];
    private volatile int putIndex = 0;
    private volatile int getIndex = 0;
    private volatile int size;

    public synchronized void put(Integer i) throws InterruptedException {
        while (isFull()) {
            try {
                System.out.println("Producer waiting...");
                wait();
            } catch (InterruptedException e) {}
        }
            array[putIndex] = i;
            movePutIndex();
            System.out.println(Arrays.toString(array));
            notifyAll();
    }


    public synchronized Integer get() throws InterruptedException {
        Integer i;
        while (!isFull()) {
            try {
                System.out.println("Consumer waiting...");
                wait();
            } catch (InterruptedException e) {}
        }
            i = array[getIndex];
            array[getIndex] = null;
            moveGetIndex();
            notifyAll();
            return i;
    }

    private boolean isFull() {
        return size == array.length;
    }

    private boolean isEmpty() {
        return size == 0;
    }

    private void movePutIndex() {
        size++;
        if (putIndex == array.length - 1) putIndex = 0;
        else putIndex++;
    }

    private void moveGetIndex() {
        size--;
        if (getIndex == array.length - 1) getIndex = 0;
        else getIndex++;
    }

    public static void main(String[] args) throws InterruptedException {

        Buffer buffer = new Buffer();

        new Thread(new Producer(buffer)).start();
        new Thread(new Producer(buffer)).start();
        new Thread(new Producer(buffer)).start();
        new Thread(new Producer(buffer)).start();

        new Thread(new Consumer(buffer)).start();
        new Thread(new Consumer(buffer)).start();
        new Thread(new Consumer(buffer)).start();
        new Thread(new Consumer(buffer)).start();

    }
}
